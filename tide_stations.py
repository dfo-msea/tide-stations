# import system and project modules
import setup
import spatial
import pathlib
import glob
import logging
import numpy as np
import traceback
import time


def tide_stations(input_path, tolerance, buffer_distance, cell_size, epsg_code, regime_distance_threshold):
    """
    Execute series of calls to functions
    :param input_path: Full path to input directory where data are located.
    :param tolerance: Tolerance for simplifying coastline geometry.
    :param buffer_distance: Distance to buffer simplified coastline by.
    :param cell_size: Cell size for vector to raster conversion of coastline.
    :param regime_distance_threshold: distance threshold between source point and tide station.
    :return:
    """
    try:
        # check if input directory exists and return windows path object
        in_dir = setup.check_dir(input_path)

        # create output directory (date stamped) at same level as input directory
        out_dir = setup.make_dir(in_dir)

        # set up to export logging information to a log file in output directory
        logger = setup.setup_logging(out_dir)

        # log params
        logging.info("PARAMETERS")
        logging.info("Input path: {}".format(input_path))
        logging.info("Simplify tolerance: {}".format(tolerance))
        logging.info("Buffer distance: {}".format(buffer_distance))
        logging.info("Cell resolution: {}".format(cell_size))
        logging.info("EPSG code: {}".format(epsg_code))
        logging.info("Distance cutoff threshold: {}".format(regime_distance_threshold))

        # timer
        start_time = time.time()

        # zip source data files
        zipf = spatial.zip_directory(in_dir, out_dir)

        # check projection systems of vector data (will check all shapefiles from project directory recursively
        # if shapefiles do not have an EPSG code, this will fail. If the EPSG code of a given shapefile is not equal to
        # EPSG: <code>, it will be reprojected and the original data will be deleted.
        spatial.check_srs(in_dir, int(epsg_code))

        # get path to vector coastline file
        vector_coast_path = pathlib.Path(in_dir / "data" / "coast")
        vector_coastline_shp = glob.glob("{}/*.shp".format(vector_coast_path))

        # get path to vector dive sites file
        vector_dives = pathlib.Path(in_dir / "data" / "dives")
        vector_dives_shp = glob.glob("{}/*.shp".format(vector_dives))

        # get path to vector chs tide stations
        vector_stations = pathlib.Path(in_dir / "data" / "stations")
        vector_stations_shp = glob.glob("{}/*.shp".format(vector_stations))

        # get path to vector tidal regimes file
        vector_regimes_path = pathlib.Path(in_dir / "data" / "regimes")
        vector_regimes_shp = glob.glob("{}/*.shp".format(vector_regimes_path))

        # load dive sites shapefile and create column for station
        dive_sites = spatial.read_shp_create_id(vector_dives_shp[0], 'sd')
        # insert column for method (a-star | tidal-regime)
        dive_sites.insert(0, "Method", "")
        # insert column for nearest chs tide station
        dive_sites.insert(0, "Station", np.nan)
        # seed and sample
        #dive_sites_sample = dive_sites.sample(n=250, random_state=123)

        # load chs stations and create unique id
        chs_stations = spatial.read_shp_create_id(vector_stations_shp[0], 'ts')

        # load tidal regimes and create unique id
        tidal_regimes = spatial.read_shp_create_id(vector_regimes_shp[0], 'rg')

        # join tidal regime poly attributes to chs stations and dive sites
        dive_sites_join = spatial.point_in_poly(dive_sites, tidal_regimes)
        chs_stations_join = spatial.point_in_poly(chs_stations, tidal_regimes)

        # simplify coastline
        coast_simple = spatial.simplify_geom(int(tolerance), vector_coastline_shp[0], out_dir)

        # buffer coastline
        coast_buffered = spatial.buffer_geom(int(buffer_distance), coast_simple, out_dir)

        # convert vector coastline to a raster at <cell size> cell resolution and project to <epsg code> projection
        cell_resolution = int(cell_size)
        raster_coast_filename = spatial.vector_to_raster(coast_buffered, cell_resolution, out_dir, int(epsg_code))

        # create array from raster
        cost_surface_array = spatial.raster2array(raster_coast_filename)  # creates array from cost surface raster

        # create graph from array and subset based on binary values
        water_graph = spatial.grid2graph_subset(cost_surface_array)

        # remove rows from dive sites and stations with no node in graph
        spatial.remove_land_points(dive_sites_join, chs_stations_join, raster_coast_filename, water_graph)

        # calculate distances from source points (dive sites) to destinations (chs tide stations)
        logging.info(
            "CALCULATING DISTANCES FROM {} SOURCE POINTS TO {} DESTINATION POINTS...".format(len(dive_sites_join),
                                                                                             len(chs_stations_join)))
        distances = [spatial.calc_distance(row, chs_stations_join) for index, row in dive_sites_join.iterrows()]
        logging.info("CALCULATING DISTANCES... COMPLETE")

        # sort joined dive sites by uniueidsd column
        dive_sites_join = dive_sites_join.sort_values("UniqueIDsd")
        # reset index in df
        dive_sites_join.reset_index(drop=True, inplace=True)
        # number of dive sites
        num_dives = len(dive_sites_join)
        try:
            for index, row in dive_sites_join.iterrows():
                # get the dive UniqueID
                dive_id = row["UniqueIDsd"]

                # coords as a tuple (x, y)
                dive_coords = (row["geometry"].x, row["geometry"].y)
                logging.info("PROCESSING DIVE {} OF {} -- UNIQUEID: {}".format(index + 1, num_dives, dive_id))

                # get list of matching tide stations
                matching_stations = spatial.get_matching_stations(row, chs_stations_join)

                # select the dict from list of distances dicts where the SourceID matches the dive UniqueID
                # use a generator expression:
                # https://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search
                distances_subset = next(item for item in distances if item["SourceID"] == dive_id)
                # get just the geodataframe (drop the sourceid element)
                distances_subset = distances_subset.get("Destinations")

                # get records where the dive site has a tide station from the same tidal regime polygon
                distances_tidal_regime = distances_subset[
                    distances_subset["UniqueIDts"].isin(list(matching_stations["UniqueIDts"]))]

                # test if matching id list is empty. if not, run astar for each record and get shortest path
                if len(matching_stations) > 0:
                    logging.info(" CALCULATING A* PATHS FOR EACH OF THE TIDE STATIONS WITHIN THE TIDAL REGIME")
                    paths_within_regimes = []
                    for station_index, station_row in matching_stations.iterrows():
                        logging.info(" STATION ID: {}".format(station_row["UniqueIDts"]))
                        station_coords = (station_row["geometry"].x, station_row["geometry"].y)
                        coords = (dive_coords, station_coords)
                        # get pixel coordinates from geom coords
                        pix_coords = spatial.pixel_coords(raster_coast_filename, coords)
                        logging.info(" * Pixel coords of source and destination: {}".format(pix_coords))
                        # generate A* path from graph and source/dest nodes
                        path = spatial.astar_path(water_graph, pix_coords[0], pix_coords[1], spatial.dist)
                        # length of path (number of nodes * cell resolution)
                        path_len = len(path) * cell_resolution
                        logging.info(" * Length of path: {}".format(path_len))
                        # dictionary with station id and path length
                        path_dict = {"id": station_row["UniqueIDts"], "path_len": path_len}
                        # append dict to list
                        paths_within_regimes.append(path_dict)
                    logging.info(" DETERMINING STATION WITHIN REGIME WITH SHORTEST A* PATH")
                    # https://stackoverflow.com/questions/5320871/in-list-of-dicts-find-min-value-of-a-common-dict-field
                    shortest_path = min(paths_within_regimes, key=lambda x: x["path_len"])
                    if shortest_path.get("path_len") < int(regime_distance_threshold):
                        # assign UniqueID from tide station to dive site record
                        tide_id = shortest_path.get("id")
                        dive_sites_join.at[index, "Station"] = tide_id
                        dive_sites_join.at[index, "Method"] = "tidal-regime"
                        logging.info(
                            " * Found a tide station (UniqueIDts = {}) within the same polygon and less than {}. "
                            "Moving to next dive site.".format(tide_id, regime_distance_threshold))
                        logging.info(" * Path length: {}".format(shortest_path.get("path_len")))
                        continue
                    else:
                        logging.info("Nearest by-water station has a distance of {}. This is greater than "
                                     "threshold of {}.".format(shortest_path.get("path_len"),
                                                               regime_distance_threshold))
                        pass
                else:
                    logging.info("There are no matching stations in the same tidal regime as source point.")

                # calculate A* path for dive sites using sorted destination points
                logging.info(">> SEARCHING FOR NEAREST STATION BY WATER <<")

                # iterate rows in distances_subset geodataframe to get geom and run A*
                counter = 1
                for index2, row2 in distances_subset.iterrows():
                    if counter <= 10:
                        # if it is the first iteration
                        if counter == 1:
                            logging.info(" STATION ID: {}".format(row2["UniqueIDts"]))
                            logging.info(" * Dive site coordinates: {}".format(dive_coords))
                            station_coords = (row2["geometry"].x, row2["geometry"].y)
                            logging.info(" * Station coords: {}".format(station_coords))
                            coords = (dive_coords, station_coords)
                            # get pixel coordinates from geom coords
                            pix_coords = spatial.pixel_coords(raster_coast_filename, coords)
                            logging.info(" * Pixel coords of source and destination: {}".format(pix_coords))
                            try:
                                # generate A* path from graph and source/dest nodes
                                path = spatial.astar_path(water_graph, pix_coords[0], pix_coords[1], spatial.dist)
                                # length of path (number of nodes * cell resolution)
                                path_len = len(path) * cell_resolution
                                logging.info(" * Length of path: {}".format(path_len))
                                logging.info(" * Cartesian distance: {}".format(row2["Distance"]))
                                station_id = row2["UniqueIDts"]
                                # get sorted index of next row and use to subset ID and distance geodataframe
                                next_row = row2["SortedIndex"] + 1
                                next_station_id = distances_subset.iloc[next_row]["UniqueIDts"]
                                next_station_dist = distances_subset.iloc[next_row]["Distance"]
                                logging.info(" * Cartesian distance of next closest station (ID {}): {}".
                                             format(next_station_id, next_station_dist))
                                if path_len < next_station_dist:
                                    # assign UniqueID from tide station to dive site record
                                    dive_sites_join.at[index, "Station"] = station_id
                                    dive_sites_join.at[index, "Method"] = "a-star"
                                    logging.info(
                                        " * Path distance is shorter than Cartesian distance of next closest "
                                        "station. Moving to next dive site.")
                                    break
                                else:
                                    # calculate nearest n paths (max of 10)
                                    top10 = spatial.nearest10paths(distances_subset,
                                                                   dive_coords,
                                                                   water_graph,
                                                                   cell_size,
                                                                   raster_coast_filename)
                                    # get station unique id with shortest path
                                    shortest_path_id = min(top10, key=top10.get)
                                    logging.info(" * Dictionary of station IDs and distances: {}".format(top10))
                                    logging.info(" * Station ID with shorest path: {}".format(shortest_path_id))
                                    # add station unique id to dive sites record
                                    dive_sites_join.at[index, "Station"] = shortest_path_id
                                    dive_sites_join.at[index, "Method"] = "a-star"
                                    break
                            except:
                                logging.error(" * Destination node is not reachable from source node.")
                        # if it is not the first iteration
                        else:
                            try:
                                # calculate nearest n paths (max of 10)
                                top_paths = spatial.nearest10paths(distances_subset,
                                                                   dive_coords,
                                                                   water_graph,
                                                                   cell_size,
                                                                   raster_coast_filename)
                                # get station unique id with shortest path
                                shortest_path_id = min(top_paths, key=top_paths.get)
                                logging.info(" * Dictionary of station IDs and distances: {}".format(top_paths))
                                logging.info(" * Station ID with shorest path: {}".format(shortest_path_id))
                                # add station unique id to dive sites record
                                dive_sites_join.at[index, "Station"] = shortest_path_id
                                dive_sites_join.at[index, "Method"] = "a-star"
                                break
                            except:
                                logging.warning(" * Failure creating path for dive site unique id: {}.".format(dive_id))
                                logging.error(traceback.format_exc())
                        counter += 1
                    else:
                        logging.warning(" * ABORTING: TRIED 10 NEAREST STATIONS AND FAILED.")
                        break
            logging.info("CALCULATING PATHS... COMPLETE")
        except Exception:
            logging.warning("Error calculating paths or getting station within tidal regime polygon...")
            logging.error(traceback.format_exc())

        finally:
            # export shapefiles of dive sites and tide_stations. These will have the uniqueid fields and the stations
            # field in the dive sites shp
            logging.info("EXPORTING SHAPEFILES...")
            spatial.gdf2shp(dive_sites_join, out_dir, "dive_sites")
            spatial.gdf2shp(chs_stations_join, out_dir, "tide_stations")
            logging.info("--- {} seconds ---".format(time.time() - start_time))
            return "Success"
    except Exception:
        logging.error(traceback.format_exc())


def main():
    # CLI arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", type=str,
                        help="Full path to input directory where data are located.")
    parser.add_argument("simplify_tolerance", type=str,
                        help="Tolerance for simplifying coastline geometry. Recommended value: 50")
    parser.add_argument("buffer_dist", type=str,
                        help="Distance to buffer simplified coastline by. Recommended value: -100")
    parser.add_argument("cell_resolution", type=str,
                        help="Cell size for vector to raster conversion of coastline. Recommended value: 150")
    parser.add_argument("epsg_code", type=str,
                        help="Numeric EPSG Code for spatial data such as 3005.")
    parser.add_argument("dist_threshold", type=str,
                        help="Threshold to use as cutoff for chs station within a tidal regime. This is the "
                             "by-water distance from a source (dive) point to CHS tide station.")
    args = parser.parse_args()

    # run program
    tide_stations(args.input_dir,
                  args.simplify_tolerance,
                  args.buffer_dist,
                  args.cell_resolution,
                  args.epsg_code,
                  args.dist_threshold)


if __name__ == "__main__":
    main()
