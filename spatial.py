# import modules
from osgeo import gdal, ogr, osr
import os
import logging
import traceback
import glob
import pathlib
from itertools import zip_longest
import geopandas as gp
import pandas as pd
from fiona.crs import from_epsg
from skimage.graph import route_through_array
import numpy as np
import networkx as nx
from zipfile import ZipFile
import rasterio as rio
import matplotlib.pyplot as plt
from datetime import datetime


# set up global variables
DATE_TIME = datetime.now().strftime("%Y-%m-%d")

# break message for log file
MESSAGE_BRK = "******************************************************************************"


# functions
def vector_to_raster(polygon, pixel_size, output_dir, output_epsg_code):
    """
    Convert a vectory polygon to a raster GeoTIFF. Output filename will have the same name as input file (besides
    the extension). Project GeoTiff to specified projection using EPSG code
    :param polygon: path to polygon file
    :param pixel_size: desired cell size for output GeoTIFF
    :param output_dir: output directory (windows Path object)
    :param output_epsg_code: epsg code for GeoTIFF
    :return: filepath to output raster
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("PROCESSING VECTOR TO RASTER...")

        # Define NoData value of new raster
        NoData_value = -9999

        # Open the vector data source and read in the extent
        logging.info("Opening {} for reading".format(polygon))
        source_ds = ogr.Open(polygon)
        source_layer = source_ds.GetLayer()
        # get extent of layer
        x_min, x_max, y_min, y_max = source_layer.GetExtent()

        # Filename of the raster GeoTIFF that will be created (temporary file and projected file)
        temp_filename = os.path.join(output_dir, 'temp.tif')
        vector_filename = source_layer.GetDescription()
        projected_filename = os.path.join(output_dir, '{}.tif').format(vector_filename)

        # Set parameters for destination data source
        logging.info("Setting parameters for output raster")
        x_res = int((x_max - x_min) / pixel_size)
        y_res = int((y_max - y_min) / pixel_size)

        # Create the destination data source
        target_ds = gdal.GetDriverByName('GTiff').Create(temp_filename, x_res, y_res, 1, gdal.GDT_Byte)
        target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))

        # assign nodata value to raster band
        logging.info("Assigning NoData value as {} for output raster".format(NoData_value))
        band = target_ds.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)

        # Rasterize vector layer (osgeo.ogr.Layer)
        logging.info("Converting {} to raster".format(vector_filename))
        gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])

        # Close dataset to write to disk
        target_ds = None

        # Project raster to EPSG <code>
        epsg_code = "EPSG:{}".format(output_epsg_code)
        logging.info("Projecting {} raster to {}".format(vector_filename, epsg_code))
        land_binary = gdal.Open(temp_filename)
        gdal.Warp(projected_filename, land_binary, dstSRS=epsg_code)
        # Calculate statistics on raster band
        logging.info("Calculating statistics for {} GeoTIFF".format(vector_filename))
        proj_binary = gdal.Open(projected_filename)
        proj_band = proj_binary.GetRasterBand(1)
        proj_band.GetStatistics(0, 1)

        # Close dataset and write to disk
        logging.info("Closing datasets and cleaning temporary files")
        land_binary = None

        # Cleanup temp file
        os.remove(temp_filename)
        logging.info("PROCESSING VECTOR TO RASTER... COMPLETE")
        return projected_filename
    except Exception:
        logging.error(traceback.format_exc())


def check_srs(input_dir, out_epsg_code):
    """
    Check spatial reference system of all shapefiles in /data subdirectory. If EPSG code does not match output_epsg_code
    then reproject the layer and export as a shapefile in whatever directory it was in (i.e. /data/coast)
    and delete the original. The source data is zipped beforehand so deleting it is fine.
    :param input_dir: input directory (project level).
    :param out_epsg_code: EPSG code https://spatialreference.org/ref/epsg/
    :return: dictionary of spatial reference system information for the layers
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("CHECKING SPATIAL REFERENCE SYSTEMS FOR VECTOR LAYERS...")
        data_subdir = pathlib.Path(input_dir / "data")

        # get list of all shapefiles from /data directory recursively
        shps = glob.glob("{}/**/*.shp".format(data_subdir), recursive=True)

        # get lists of datasets, paths, layers, names, srs information
        datasets = [ogr.Open(shp) for shp in shps]
        paths = [os.path.dirname(ds) for ds in shps]
        layers = [ds.GetLayer() for ds in datasets]
        names = [lyr.GetDescription() for lyr in layers]
        srs = [lyr.GetSpatialRef() for lyr in layers]
        srs_names = [sr.GetName() for sr in srs]
        srs_cstype = []
        for sr in srs:
            if sr.IsGeographic() == 1:  # this is a geographic srs
                cstype = "GEOGCS"
                srs_cstype.append(cstype)
            else:  # this is a projected srs
                cstype = "PROJCS"
                srs_cstype.append(cstype)

        # get EPSG codes
        srs_epsg = [sr.GetAuthorityCode(cstype) for sr, cstype in zip(srs, srs_cstype)]

        # create dictionary of spatial reference system information
        srs_dict = {name: (layer, path, epsg_code) for name, layer, path, epsg_code in zip_longest(names,
                                                                                                   layers,
                                                                                                   paths,
                                                                                                   srs_epsg)}

        # list of shapefiles to delete if data gets reprojected
        to_delete = []

        # loop through layers and check projection information
        counter = 1
        for shp, name, layer, path, epsg_code in zip(shps, names, layers, paths, srs_epsg):
            logging.info("{}  Name: {}".format(counter, name))
            logging.info("   Path: {}".format(path))
            logging.info("   EPSG Code: {}".format(epsg_code))
            if int(epsg_code) == int(out_epsg_code):
                logging.info("   Reprojection: not required")
            else:
                logging.warning("   Reprojection: required. EPSG code {} for {} does not match EPSG {} projection."
                                .format(epsg_code, name, out_epsg_code))
                reproject_layer(shp, name, path, out_epsg_code)
                to_delete.append(shp)
            # increment counter
            counter += 1

        # Save and close the shapefiles
        datasets = None
        # release reference to paths
        shps = None

        # if any layers were reprojected, delete the original
        if to_delete:
            # delete source data that has been projected and saved in same location
            driver = ogr.GetDriverByName("ESRI Shapefile")
            for file in to_delete:
                if os.path.exists(file):
                    logging.warning("Deleting {} because it has been reprojected. See zipped source data for"
                                    "original data.".format(file))
                    driver.DeleteDataSource(file)
        logging.info("CHECKING SPATIAL REFERENCE SYSTEMS FOR VECTOR LAYERS... COMPLETE")
        return srs_dict
    except Exception:
        logging.error(traceback.format_exc())


def get_file_paths(input_dir):
    """
    FUNCTION NO LONGER USED
    Get a list of file paths by walking input directory and then return all file paths
    :param input_dir: input directory
    :return: list of file paths in data directory to zip.
    """
    try:
        # logging messages
        logging.info("GETTING FILE PATHS IN {} TO ARCHIVE IN A ZIP FILE".format(input_dir))

        # initializing empty file paths list
        file_paths = []

        # walk through directory and subdirectories, adding files to list zip
        for root, directories, files in os.walk(input_dir):
            for filename in files:
                # join the two strings in order to form the full filepath.
                filepath = os.path.join(root, filename)
                file_paths.append(filepath)

        # returning all file paths
        logging.info("GETTING FILE PATHS... COMPLETE")
        return file_paths
    except Exception:
        logging.error(traceback.format_exc())


def zip_directory(input_dir, output_dir):
    """
    Zip files in file_paths list into a zip file
    :param input_dir: input directory (root of project folder)
    :param output_dir: output directory
    :return: zip file location
    """
    try:
        # logging messages
        logging.info(MESSAGE_BRK)
        logging.info("ZIPPING SOURCE DATA...")
        # data subdirectory to walk through and get list of files to zip
        data_subdir = pathlib.Path(input_dir / "data")

        # create a ZipFile object
        with ZipFile(output_dir / "source-data-{}.zip".format(DATE_TIME), "w") as zipObj:
            # Iterate over all the files in directory
            for folderName, subfolders, filenames in os.walk(data_subdir):
                for filename in filenames:
                    # create complete filepath of file in directory
                    filePath = os.path.join(folderName, filename)
                    # Add file to zip
                    zipObj.write(filePath, os.path.basename(filePath))

        logging.info("ZIPPING SOURCE DATA... COMPLETE")
        return zipObj
    except Exception:
        logging.error(traceback.format_exc())


def reproject_layer(filepath, name, out_dir, to_epsg_code):
    """
    Reproject layer using GeoPandas to_crs https://geopandas.org/reference.html#geopandas.GeoDataFrame.to_crs
    :param filepath: path to shapefile
    :param name: name of layer
    :param out_dir: output directory
    :param to_epsg_code: output EPSG code for spatial reference system
    :return: path of output shapefile
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("REPROJECTING DATA...")
        data = gp.read_file(filepath)
        data_proj = data.copy()

        # reproject the geometries by replacing the values with projected ones
        data_proj['geometry'] = data_proj['geometry'].to_crs(int(to_epsg_code))
        data_proj.crs = from_epsg(int(to_epsg_code))

        # create the output layer
        out_name = "{}_{}".format(name, to_epsg_code)
        logging.info("Creating output layer for {}".format(out_name))
        out_path = pathlib.Path(out_dir) / out_name
        output_shapefile = "{}.shp".format(out_path)

        # write shp file
        logging.info("Writing {} to disk".format(out_name))
        data_proj.to_file(output_shapefile)
        logging.info("REPROJECTING DATA... COMPLETE")
        logging.info(MESSAGE_BRK)

        # release reference for data
        data = None
        return output_shapefile
    except Exception:
        logging.error(traceback.format_exc())


def simplify_geom(tolerance, filepath, out_dir):
    """
    Read a shapefile (load as a pandas GeoDataFrame) and simplify geometry
    :param tolerance: integer All points in a simplified geom will be no more than tolerance distance from the original.
    :param filepath: path to shapefile
    :param out_dir: output directory
    :return: class 'geopandas.geodataframe.GeoDataFrame' GeoDataFrame with additional column UniqueID.
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("Loading {} as GeoDataframe...".format(os.path.basename(filepath)))
        geodataframe = gp.read_file(filepath)
        logging.info("Simplifying geometry...")
        simplified = geodataframe.simplify(int(tolerance))
        out_name = os.path.basename(filepath).split(".")[0] + "_simplified.shp"
        out_path = os.path.join(out_dir, out_name)
        logging.info("Exporting geodataframe as shapefile to {}".format(out_name))
        simplified.to_file(out_path)
        return out_path
    except Exception:
        logging.error(traceback.format_exc())


def buffer_geom(distance, filepath, out_dir):
    """
    Buffer a shapefile by specified distance
    :param distance: integer (positive or negative)
    :param filepath: path to spatial vector data
    :param out_dir: output directory
    :return: path to output file
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("Loading {} as GeoDataframe...".format(os.path.basename(filepath)))
        geodataframe = gp.read_file(filepath)
        logging.info("Buffering geometry by {}...".format(distance))
        buffered = geodataframe.buffer(int(distance))
        out_name = os.path.basename(filepath).split(".")[0] + "_buff.shp"
        out_path = os.path.join(out_dir, out_name)
        logging.info("Exporting geodataframe as shapefile to {}".format(out_name))
        buffered.to_file(out_path)
        return out_path
    except Exception:
        logging.error(traceback.format_exc())


def raster2array(raster_path):
    """
    Convert a raster to an array. Assumes single-band raster and will only return first band if more than 1.
    :param raster_path: path to raster file
    :return: object of class type numpy.ndarray
    """
    try:
        raster = gdal.Open(raster_path)
        band = raster.GetRasterBand(1)
        array = band.ReadAsArray()
        return array
    except Exception:
        logging.error(traceback.format_exc())


def reshape_array(array):
    """
    FUNCTION NO LONGER USED.
    Reshape array to a square (same dimensions).
    :param array: numpy.ndarray object
    :return: array with equal number of elements in each dimension. Required for networkx graph.
    """
    try:
        logging.info("RESIZING ARRAY TO MAX NUMBER OF ELEMENTS FOR EACH DIMENSION...")
        max_elements = max(array.shape)
        square_array = np.resize(array, (max_elements, max_elements))
        return square_array
    except Exception:
        logging.error(traceback.format_exc())


def coord2pixelOffset(rasterfn, x, y):
    """
    FUNCTION NO LONGER USED.
    Get the pixel location (indices) from coordinates
    https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html
    :param rasterfn: input raster file path
    :param x: x coordinate
    :param y: y coordinate
    :return:
    """
    try:
        raster = gdal.Open(rasterfn)
        geotransform = raster.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
        xOffset = int((x - originX) / pixelWidth)
        yOffset = int((y - originY) / pixelHeight)
        return xOffset, yOffset
    except Exception:
        logging.error(traceback.format_exc())


def createPath(CostSurfacefn, costSurfaceArray, startCoord, stopCoord):
    """
    FUNCTION NO LONGER USED
    :param CostSurfacefn:
    :param costSurfaceArray:
    :param startCoord:
    :param stopCoord:
    :return:
    """
    # coordinates to array index
    startCoordX = startCoord[0]
    startCoordY = startCoord[1]
    startIndexX, startIndexY = coord2pixelOffset(CostSurfacefn, startCoordX, startCoordY)

    stopCoordX = stopCoord[0]
    stopCoordY = stopCoord[1]
    stopIndexX, stopIndexY = coord2pixelOffset(CostSurfacefn, stopCoordX, stopCoordY)

    # create path
    indices, weight = route_through_array(costSurfaceArray,
                                          (startIndexY, startIndexX),
                                          (stopIndexY, stopIndexX),
                                          geometric=False,
                                          fully_connected=True)
    # Transpose array: https://scikit-image.org/docs/dev/api/skimage.graph.html
    indices = np.stack(indices, axis=-1)
    # return an array of given shape filled with zeros
    path = np.zeros_like(costSurfaceArray)
    # fill indices of path with value of 1
    path[indices[0], indices[1]] = 1
    return path


def array2raster(newRasterfn, rasterfn, array):
    """
    Convert an array to a raster
    :param newRasterfn: output file name
    :param rasterfn: input file name used to define profile for output
    :param array: array to convert to raster
    :return:
    """
    try:
        raster = gdal.Open(rasterfn)
        geotransform = raster.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
        cols = array.shape[1]
        rows = array.shape[0]

        driver = gdal.GetDriverByName('GTiff')
        outRaster = driver.Create(newRasterfn, cols, rows, gdal.GDT_Byte)
        outRaster.SetGeoTransform((originX, pixelWidth, 0, originY, 0, pixelHeight))
        outband = outRaster.GetRasterBand(1)
        outband.WriteArray(array)
        outRasterSRS = osr.SpatialReference()
        outRasterSRS.ImportFromWkt(raster.GetProjectionRef())
        outRaster.SetProjection(outRasterSRS.ExportToWkt())
        outband.FlushCache()
        return newRasterfn
    except Exception:
        logging.error(traceback.format_exc())


def array2netxgraph(array):
    """
    Convert a numpy.ndarray object to a NetworkX Graph
    :param array: numpy array
    :return: graph object
    """
    try:
        # convert numpy array to a NetworkX Graph
        logging.info("CONVERTING NUMPY ARRAY TO NETWORKX GRAPH...")
        graph = nx.from_numpy_array(array)
        if graph:
            logging.info("CONVERTING NUMPY ARRAY TO NETWORKX GRAPH...COMPLETE")
        return graph
    except Exception:
        logging.error(traceback.format_exc())


def calc_distance(source_point, destinations):
    """
    Calculate Cartesian distance from source point to each destination point. Append new column "Distance" to
    geodataframe and sort by shortest distance.
    :param source_point: class 'pandas.core.series.Series (One-dimensional ndarray) in order to access the
                         shapely geometry to use the distance method
                         (https://geopandas.org/reference.html#geopandas.GeoSeries.distance). This is the source point
                         used to calculate distance from.
    :param destinations: class 'geopandas.geodataframe.GeoDataFrame'. A GeoDataFrame with point locations. Each point
                         will be used as the destination point in the distance calculation.
    :return: dest_dict: dictionary with source id and list of destinations geodataframes with distances.
                        Distances are the distance to each of the destination points.
    """
    try:
        source_geom = source_point.geometry
        # calculate distances from source to each destination point
        distances = [row.geometry.distance(source_geom) for index, row in destinations.iterrows()]
        # append Distance column to geodataframe
        destinations = destinations.assign(Distance=distances)
        # sort geodataframe by Distance column by shortest distance from source to destination
        destinations = destinations.sort_values("Distance")
        # add sorted index column
        destinations.insert(0, "SortedIndex", range(1, 1 + len(destinations)))
        # dictionary with source id and list of destinations
        dest_dict = {"SourceID": source_point["UniqueIDsd"], "Destinations": destinations}
        return dest_dict
    except Exception:
        logging.error(traceback.format_exc())


def read_shp_create_id(filepath, id_name):
    """
    Read a shapefile (load as a pandas GeoDataFrame) and add a UniqueID column. Populate 1: length of GeoDataFrame.
    :param filepath: path to shapefile
    :param id_name: name to append to unique_id. should be 2 characters max
                    rg == tidal regime
                    ts == tide station
                    sd == shelfish dive site
    :return: class 'geopandas.geodataframe.GeoDataFrame' GeoDataFrame with additional column UniqueID.
    """
    try:
        logging.info(MESSAGE_BRK)
        logging.info("Loading {} as GeoDataframe...".format(os.path.basename(filepath)))
        geodataframe = gp.read_file(filepath)
        logging.info("Adding UniqueID column to GeoDataframe. Values will be 1-{}...".format(len(geodataframe)))
        # add uniqueid column to identify records (FID and OBJECTID columns appear to be dropped from shapefiles)
        geodataframe.insert(0, 'UniqueID{}'.format(id_name), range(1, 1 + len(geodataframe)))
        return geodataframe
    except Exception:
        logging.error(traceback.format_exc())


def point_in_poly(points_gdf, poly_gdf):
    """
    Spatial join points within polygons. Attach poly attributes to points. Export as shp.
    :param points_gdf: points geodataframe
    :param poly_gdf: polygons geodataframe
    :return: points geodataframe
    """
    try:
        logging.info(MESSAGE_BRK)
        # Spatially join points that fall within polygon
        logging.info("Spatially joining points that fall within polygon...")
        npoints = len(points_gdf)
        logging.info("{} points before join...".format(npoints))
        sjoined = gp.sjoin(points_gdf, poly_gdf, op='within')
        npoints_after = len(sjoined)
        logging.info("{} points after join...".format(npoints_after))
        logging.info("{} points removed during spatial join...".format(npoints - npoints_after))
        return sjoined

    except Exception:
        logging.error(traceback.format_exc())


def get_matching_stations(dive_site, stations_gdf):
    """
    Get id for the polygon the site is within. Select stations with matching uniqueid (if any, may be >1).
    :param dive_site: row (dive site) in geopandas dataframe
    :param stations_gdf: geopandas dataframe of chs stations
    :return: list of tide station rows (if any) that are within the same poly as the dive site
    """
    try:
        logging.info("Searching for stations within the same tidal regime as the source point...")

        # search for station(s) with matching polygon id
        reg_id = dive_site["UniqueIDrg"]
        stations_ids = list(stations_gdf[stations_gdf["UniqueIDrg"] == reg_id]["UniqueIDts"])
        logging.info("Found {} tide station(s) inside the same tidal regime polygon as the dive point with "
                     "unique ID: {}.".format(len(stations_ids), dive_site["UniqueIDsd"]))
        station_subset = stations_gdf[stations_gdf["UniqueIDts"].isin(stations_ids)]
        return station_subset

    except Exception:
        logging.error(traceback.format_exc())


def grid2graph_subset(array):
    """
    Create a Graph of nodes and edges (networkx) using the shape attribute of array.
    Subset the Graph by removing nodes where the corresponding array value == 1. Keep only 0 values.
    :param array:
    :return: subset of Graph where values == 1 from the input array are excluded (land values)
    """
    try:
        # generate graph - same shape as array
        logging.info(MESSAGE_BRK)
        logging.info("Creating graph from array...")
        G = nx.grid_2d_graph(*array.shape)

        # remove those nodes where the corresponding value is != 0
        logging.info("Removing nodes from graph where array values are equal to 1 (land)...")
        for val, node in zip(array.ravel(), sorted(G.nodes())):
            if val != 0:
                G.remove_node(node)

        # return G
        logging.info(MESSAGE_BRK)
        return G
    except Exception:
        logging.error(traceback.format_exc())


def dist(a, b):
    """
    A function to evaluate the estimate of the distance from the a node to the target. The function takes two nodes
    arguments and must return a number.
    https://networkx.org/documentation/networkx-2.4/reference/algorithms/generated/networkx.algorithms.shortest_paths.astar.astar_path.html
    :param a:
    :param b:
    :return:
    """
    (x1, y1) = a
    (x2, y2) = b
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5


def pixel_coords(raster_filename, coordinates):
    """
    Get pixel coordinates from projected coordinates of source and destination points
    :param raster_filename:
    :param coordinates: tuple of coordinates ((source x, source y), (dest x, dest y))
    :return: list of pixel coordinates
    """

    # coords list of tuples (coords)
    coords_list = []

    # Open the raster
    with rio.open(raster_filename) as dataset:
        # Loop through your list of coords
        for i, (x_coord, y_coord) in enumerate(coordinates):
            # Get pixel coordinates from map coordinates
            pixel_coords = (dataset.index(x_coord, y_coord))
            coords_list.append(pixel_coords)
    return coords_list


def astar_path(graph, src, dest, heuristic):
    """
    Calculate shortest path using A* algorithm.
    https://networkx.org/documentation/networkx-2.4/reference/algorithms/generated/networkx.algorithms.shortest_paths.astar.astar_path.html
    :param graph: subsetted Graph
    :param src: source node (tuple of pixel coords)
    :param dest: destination node (tuple of pixel coords)
    :param heuristic: A function to evaluate the estimate of the distance from the a node to the target.
    :return: path (list of nodes from source to destination)
    """
    try:
        # A* algorthim for path finding
        shortest_path = nx.astar_path(graph, src, dest, heuristic, weight='weight')
        return shortest_path
    except Exception:
        logging.error(traceback.format_exc())


def draw_path(graph, path, dive_id, out_dir):
    """
    Graph path
    :param graph: networkx graph
    :param path: path (subset of graph)
    :param dive_id: dive id number
    :param out_dir: output dir
    :return: path to output png
    """
    try:
        pos = {(x, y): (y, -x) for x, y in path}
        path_graph = graph.subgraph(path)
        fig = plt.figure(figsize=(12, 12))
        nx.draw(path_graph, pos=pos,
                node_color='red',
                width=4,
                node_size=1)
        out_path = os.path.join(out_dir, "{}.png".format(dive_id))
        plt.savefig(out_path, bbox_inches="tight")
        plt.close()
        return out_path
    except Exception:
        logging.error(traceback.format_exc())


def graph2shp(dive_id, graph, out_dir):
    """
    Export networkx graph as shapefile
    :param graph: networkx graph
    :param out_dir: output directory windows Path object
    :return: success
    """
    try:
        # create subdir
        out_name = "{}".format(dive_id)
        out_path = os.path.join(out_dir, out_name)
        os.mkdir(out_path)

        # Make path object input directory and output folder with datestamp
        logging.info("Export path as shapefile...")
        nx.write_shp(graph, out_path)
        return "Success"
    except Exception:
        logging.error(traceback.format_exc())


def gdf2shp(geodataframe, out_dir, name):
    """
    Convert geodatafram to shapefile
    :param geodataframe: geopandas geodataframe
    :param out_dir: output directory
    :param name: name for output shapefile
    :return: success
    """
    try:
        geodataframe.to_file(os.path.join(out_dir, "{}.shp".format(name)))
        return "Success"
    except Exception:
        logging.error(traceback.format_exc())


def nearest10paths(distances_gdf, src_coords, netx_graph, cell_size, raster_filename):
    """
    Calculate nearest paths for (max) 10 paths. Will be less if no nodes exist for station.
    :param distances_gdf: geodataframe of distances
    :param src_coords: source coords tuple
    :param netx_graph: graph
    :param cell_size: cell resolution
    :param raster_filename: raster file path
    :return: dict with ids and paths
    """
    try:
        # subset top 10 records
        top10 = distances_gdf[:10]
        # record is a tuple, so get the 2nd element from the tuple (series) and get the ID and other values
        ids = [record[1]["UniqueIDts"] for record in top10.iterrows()]
        # get list of destination coords
        dest_coords = [(distance["geometry"].x, distance["geometry"].y) for i, distance in top10.iterrows()]
        # get list of source and destination coords
        coords = [(src_coords, dest) for dest in dest_coords]
        # get list of pixel coords
        pix_coords = [pixel_coords(raster_filename, coord) for coord in coords]
        logging.info(" * Path distance is not shorter than Cartesian distance of next "
                     "closest station. Calculating paths for nearest {} stations.".format(len(pix_coords)))
        # get list of paths for nearest n stations
        paths = []
        for pix in pix_coords:
            try:
                p = astar_path(graph=netx_graph, src=pix[0], dest=pix[1], heuristic=dist)
                paths.append(p)
            except nx.NetworkXNoPath:
                logging.warning("No path...")
        # get list of path distances
        path_distances = [len(path) * int(cell_size) for path in paths]
        # create dictionary with ids and length of paths
        path_dicts = dict(zip(ids, path_distances))
        return path_dicts
    except Exception:
        logging.error(traceback.format_exc())


def remove_land_points(dives_gdf, stations_gdf, raster_filename, netx_graph):
    """
    Remove and rows from geodataframes where the point falls on land (no node in netx graph)
    :param dives_gdf: shellfish geodataframe
    :param stations_gdf: stations geodataframe
    :param raster_filename: path to binary raster
    :param netx_graph: graph
    :return: geodataframes for dives and stations with points removed
    """
    try:
        logging.info(" * CHECKING FOR LOCATIONS ON LAND WITH NO NODE...")
        # number of records before deleting
        number_dives = len(dives_gdf)
        number_stations = len(stations_gdf)

        # get a list of all dive coordinates and station coordinates
        dive_coords = [(record["geometry"].x, record["geometry"].y) for i, record in dives_gdf.iterrows()]
        station_coords = [(record["geometry"].x, record["geometry"].y) for i, record in stations_gdf.iterrows()]

        # convert dive and station coords to pixel locations
        # coords list of tuples (coords)
        pixel_coords_dives = []
        pixel_coords_stations = []

        # Open the raster
        with rio.open(raster_filename) as dataset:
            # Loop through list of dive site coordinates
            for i, (x_coord, y_coord) in enumerate(dive_coords):
                # Get pixel coordinates from map coordinates
                pixel_coords = (dataset.index(x_coord, y_coord))
                pixel_coords_dives.append(pixel_coords)
            # Loop through list of station coordinates
            for i, (x_coord, y_coord) in enumerate(station_coords):
                # Get pixel coordinates from map coordinates
                pixel_coords = (dataset.index(x_coord, y_coord))
                pixel_coords_stations.append(pixel_coords)

        # get list of indices not in graph
        dive_indices = [index for index, pix in enumerate(pixel_coords_dives) if pix not in netx_graph.nodes]
        station_indices = [index for index, pix in enumerate(pixel_coords_stations) if pix not in netx_graph.nodes]

        # drop rows from dives that have no node
        rows_dives = dives_gdf.index[dive_indices]
        dives_gdf.drop(rows_dives, inplace=True)
        # drop rows from stations that have no node
        rows_stations = stations_gdf.index[station_indices]
        stations_gdf.drop(rows_stations, inplace=True)

        # log results
        logging.info(" * {} dive sites removed...".format(number_dives - len(dives_gdf)))
        logging.info(" * {} CHS Stations removed...".format(number_stations - len(stations_gdf)))
        logging.info(" * CHECKING LOCATIONS... COMPLETE")
        return dives_gdf, stations_gdf
    except Exception:
        logging.error(traceback.format_exc())
