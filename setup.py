# import modules
import os
import logging
import traceback
import pathlib
import sys
from datetime import datetime


# set up global variables
DATE_TIME = datetime.now().strftime("%Y-%m-%d")


def check_dir(input_dir):
    """
    Verify that directory exists.
    :param input_dir: input path from user.
    :return: Path object to input directory.
    """
    try:
        input_path = pathlib.Path(input_dir)
        print("Input directory: %s" % input_path)
        if input_path.exists():
            return input_path
        else:
            print("{} does not exist. Exiting program.".format(input_dir))
            sys.exit()
    except Exception:
        logging.error(traceback.format_exc())


def make_dir(input_path):
    """
    Make an output directory in the parent dir of input path. Name dir: output-<datestamp>.
    :param input_dir: WindowsPath object of input directory
    :return: WindowsPath object output directory
    """
    try:
        out_name = "output-{}".format(DATE_TIME)
        # Make path object input directory and output folder with datestamp
        outpath = input_path / out_name
        logging.info(outpath)
        # Make output directory if it does not exist already
        if not outpath.exists():
            print("Creating output directory: {}".format(outpath))
            os.mkdir(outpath)
        else:
            print("{} already exists.".format(outpath))
        return outpath
    except Exception:
        logging.error(traceback.format_exc())


def setup_logging(out_dir):
    """
    Set logging parameters in output directory.
    :param out_dir: WindowsPath object
    :return: WindowsPath object for log file
    """
    try:
        # Name for log file
        log_filepath = out_dir / "tide-stations-{}.log".format(DATE_TIME)
        # Remove existing handlers for logging
        # https://stackoverflow.com/questions/30861524/logging-basicconfig-not-creating-log-file-when-i-run-in-pycharm
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        # If log file exists, append to it
        if log_filepath.is_file():
            print("Log file: {} already exists. \nMessages will be appended to it.".format(log_filepath))
            logging.basicConfig(filename=log_filepath,
                                filemode="a",
                                format="%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s",
                                level=logging.INFO)
            console = logging.StreamHandler()
            console.setLevel(logging.ERROR)
            logging.getLogger("").addHandler(console)
            logging.info("------------------------->>>>> APPENDING <<<<<-------------------------")
        # Else set up logging with new file
        else:
            print("Messages will be directed to log file: {}.".format(log_filepath))
            logging.basicConfig(filename=log_filepath,
                                filemode="w",
                                format="%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s",
                                level=logging.INFO)
            console = logging.StreamHandler()
            console.setLevel(logging.ERROR)
            logging.getLogger("").addHandler(console)
            logging.info("------------------------->>>>> LOGGING <<<<<-------------------------")
        return log_filepath
    except Exception:
        logging.error(traceback.format_exc())

