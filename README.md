# Identify Nearest By-Water Tide Station

__Main authors:__  Cole Fields & Kayleigh Gillespie  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Execution](#execution)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The main objective of this project is to identify a CHS tide station for each of the input Dive Data points. The purpose of this is to apply tide corrections to the attributes of 
the dive sites.


## Summary
The code will load data (tidal regimes, dive survey points, chs tide stations, and a land polygon). It will join the dive points and tide stations 
with the tidal regime polygons to give each point a value for the regime it falls within. Points not located within any tidal regime are dropped from the analysis.
Distances are calculated from each source point (dives) to each destination point (stations), and sorted in ascending order. Next, it will begin iterating through the dive points, 
attempting to locate tide stations within the same tidal regime. If there are any, a path is created using the A* search algorithm (avoiding land). The shortest path is identified 
and compared against the distance threshold parameter. If the path length is less than the distance threshold, the ID for the station is appended to the dive point. 
If not, the search algorithm is run on the nearest stations (max of 10) using their Cartesian distance, regardless of the tidal regimes. If successful, the station ID that has the 
shortest path will be appended to the dive site.

### The outputs of this tool are as follows:
1. Simplified vector coastline

2. Buffered version of simplified vector coastline (buffered inland)

3. Binary GeoTIFF of buffered simplified coastline where land values == 1 and water values == 0

4. Shapefile of tide stations with UniqueIDts field to identify them. There are duplicate names in the source data for some records which is why this field is created.

5. Shapefile of dive sites with UniqueIDsd and Station (if found). Station is the UniqueIDts field in the output stations shapefile. Shapefile will have column 'Method' with 
values of 'a-star' or 'tidal-regime' depending on the method used to identify the station.

6. Log file of processing results.

7. Zip file of source data (any shapefiles in `/data` directory of project.

## Status
In-development


## Contents
### setup.py 
* Module with functions to set up directories and log file

### spatial.py
* Module with functions to run spatial analysees such as projecting data, converting formats, and running A* search.

### tide_stations.py
* Calls to functions from setup and spatial modules.
* Script with main function.
* Should be executed from the `/code` directory. An output directory will be created at the parent level, in the project directory. 

## Methods
#### Setup
* Check input directory and create an output directory for results.
* Starts a log file in output directory.
* Zips source data (every shapefile in `/data` directory of project) and outputs zip file in output directory.

#### Data Preparation & Conversion
* Checks spatial reference system for each of the vector datasets. If any of them do not match the desired EPSG Code, they will be reprojected. The reprojected layers **replace** the original in their subdirectory within `/data`. However, the original source 
data is zipped and stored in the `output` directory.
* Loads shapefiles. For the dive sites and chs stations, unique id values are generated. These are 1 to the number of records in each shapefile.
* Joins tide stations and dive sites with tidal regimes to get the ID of the tidal regime that each point is within.
* Any point without a tidal regime ID are removed from the analysis. These are points that do not fall on a tidal regime polygon.
* Simplifies vector coastline.
* Buffers simplified vector coastline by specified distance (i.e. inland -100). The inland buffer is required since many of the source points fall on cells that 
are converted to land values based on the simplification, and cell resolution.
* Converts vector to raster at specified cell resolution.
* Creates a numpy array from raster.
* Creates a networkx graph from the array and removes nodes where the corresponding raster cell has a value equal to 1 (land).
This is to ensure that the path only uses the nodes where the cell values are water.
* Removes any dive sites and stations that do not have a node. These rows are deleted from the geodataframe and excluded from the analysis.

#### Analysis - Searching within Tidal Regimes & Running A* Search Algorithm
* Calculates Cartesian distance from each of the source points (dive sites) to each of the destination sites (tide stations) and sorts them. A sorted
index field is added to each of the GeoPandas GeoDataframes. 
* Begins iterating through dive site points.
* First, searches for any tide stations that are in the same tidal regime polygon. If there are none, continue and begin A* search. For any stations within the same tidal regime, calculate the shortest path to 
each, using A* search. Identify the shortest of all the paths and compare its length to the distance threshold. If it is less than the threshold, use that station (add station id to Station row and 'tidal-regime' to Method column. 
If not, run A* on nearest 10 stations and select the shortest adding station id to Station and 'a-star' to Method column. 
* Run A* search algorithm from source point (dive site) to destination (tide station) in sorted order, attempting to find nearest by-water station.
* During the first iteration, if the path to the nearest station is shorter than the Cartesian distance to the next closest station, then the closest by-water station has been located 
and the ID will be appended to the dive sites data.
* If the path is not shorter than the cartesian distance to the next closest station, then the paths to the nearest 10 stations are generated and the shortest path and corresponding station ID are identified.
* If either the source or destination node are located on a land cell, there will be no node for that location and no path will be generated. The output shapefile will have a value of 0 (zero) for dive sites 
where there was no station identified. This should only occur when the source and destination nodes are in the graph, but the destination node is not reachable from the source node.
* The algorithm will run a max of 10 times for each source point before abandoning and moving on to the next site. This is to ensure that the code is somewhat 
efficient and does not run through all destination points. This is kind of an error catch where it won't typically run more than once. The cases where it does are if a source point is 'land-locked' because the surrounding 
cells were converted to land. In this case, no destination stations are reachable.
* Once complete, shapefiles for the dive sites and tide stations will be exported to the output directory.

## Requirements
### Environment 
* python3 environment with a number of packages installed
* Windows environment

### Data
* Vector coastline shapefile (polygon). Only tested with a dissolved polygon (singe multi-polygon feature). Located in `/data/coast` directory.
* Vector dive sites (points). Located in `/data/dives` directory.
* Vector tidal regimes (polygons). Located in `/data/regimes` directory.
* Vector tide stations (points). Located in `/data/stations` directory.

### Directory Structure
* If the name is within <>, it can be changed. 
* There should be only 1 shapefile in each subdirectory (coast, dives, regimes, stations). 
* The name of the shapefiles do not matter.

```
/---<tide-stations>   
    /---data  
        /---coast   
        |       <polygon-name>.shp   
        |          
        /---dives
	|	<point-name>.shp  
        |
	/---regimes
        |       <polygon-name>.shp   
        |          
        /---stations
	        <polygon-name>.shp  
    |   
    /---code   
```

## Execution
#### Clone directory.

`git@gitlab.com:dfo-msea/tide-stations.git`

#### Navigate into directory.

`cd tide-stations`

Create a new python 3 environment and install required libraries. Replace 'env' with the a name for your new env. If you do not supply a new name, the 
virtual env name will be `tides`.

`conda env create --name <env> -f requirements.yml`

#### Activate new virtual env.

`conda activate <env>`

#### Run code.

**Help**

List the required arguments.

`python run.py -h`

```
usage: tide_stations.py [-h]
                        input_dir simplify_tolerance buffer_dist
                        cell_resolution epsg_code dist_threshold

positional arguments:
  input_dir           Full path to input directory where data are located.
  simplify_tolerance  Tolerance for simplifying coastline geometry.
                      Recommended value: 50
  buffer_dist         Distance to buffer simplified coastline by. Recommended
                      value: -100
  cell_resolution     Cell size for vector to raster conversion of coastline.
                      Recommended value: 150
  epsg_code           Numeric EPSG Code for spatial data such as 3005.
  dist_threshold      Threshold to use as cutoff for chs station within a
                      tidal regime. This is the Cartesian distance from a
                      source (dive) point to CHS tide station.

optional arguments:
  -h, --help          show this help message and exit
```

**Example of running code**

`python run.py D:\projects\tide-stations 50 -100 150 3005`


## Caveats
* If a large negative buffer value is used, there is the possibility of creating islands that do not actually exist, and opening up a new channel of water cells that the path 
may traverse. This has the potential to lead to unexpected results if not considered.
* Dive sites and/or CHS stations will be **deleted** if they fall on a land cell in the binary raster since there is no corresponding node in the graph. 
This means that there may be fewer records in the output shapefile(s) than the source data.
* Assumes a Windows environment.
* Generating the NetworkX graph is time-consuming and requires sufficient RAM (32GB+). For example on a Machine such as this with 32GB of RAM, creating the graph from a raster 
with a cell size of 150 m took about 20 minutes: 

```
System Model:              Precision 5820 Tower
System Type:               x64-based PC
Processor(s):              1 Processor(s) Installed.
                           [01]: Intel64 Family 6 Model 85 Stepping 4 GenuineIntel ~3696 Mhz
Total Physical Memory:     32,441 MB
```

## Uncertainty
* Assumes that the shortest by-water path will be from one of the 10 stations with the nearest Cartesian distance.
* In output dive-sites.shp, a value of 0 in the Station column means that a station was not identified for that dive point.

## Acknowledgements
* Ashley Park
* Robert Skelly
* Jessica Nephin


## References
https://networkx.org/documentation/networkx-1.10/reference/generated/networkx.algorithms.shortest_paths.astar.astar_path.html

https://stackoverflow.com/questions/62671695/convert-0-1-matrix-to-a-2d-grid-graph-in-python

https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html

https://gdal.org/python/

https://gis.stackexchange.com/questions/20298/is-it-possible-to-get-the-epsg-value-from-an-osr-spatialreference-class-using-th

https://pcjericks.github.io/py-gdalogr-cookbook/projection.html

https://gis.stackexchange.com/questions/28583/gdal-perform-simple-least-cost-path-analysis

https://geopandas.org/reference.html

https://thispointer.com/python-how-to-create-a-zip-archive-from-multiple-files-or-directory/ 


